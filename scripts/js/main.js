const mainDiv = document.getElementById("mainDiv");
const form = document.forms[0];
form.addEventListener("submit", addUser);
const users = [];


function setMainDivInnerHTML(arg) {
    mainDiv.innerHTML += arg;
}

// setMainDivInnerHTML("<p>Test</p>")

class User {
    constructor(id, pseudo, mail, password) {
        this.id = id;
        this.pseudo = pseudo;
        this.mail = mail;
        this.password = password;
    }

}

function hasNumber(myString) {
    return /\d/.test(myString);
}

function formValid(i) {
    let name = i[0].value;
    let mail = i[1].value;
    let password = i[2].value;
    const mails = [];
    for (let user of users) {
        mails.push(user.mail);
    }
    const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (mails.includes(mail)) {
        console.log("Mail déjà utilisé");
        return false;
    } else if (!mail.match(validRegex)) {
        console.log("Mail invalide");
        return false;
    } else if (name === "") {
        console.log("Nom invalide");
        return false;
    } else if (password === "") {
        console.log("Le mot de passe ne peut pas être vide");
        return false;
    } else if (password.toLowerCase() === password || !hasNumber(password)) {
        console.log("Mot de passe doit avoir une majuscule et un chiffre");
        return false;
    }
    return true;
}

function addUser() {
    const infos = [document.getElementsByName("formInfos")][0];
    if (formValid(infos)) {
        users.push(new User(users.length, infos[0].value, infos[1].value, infos[2].value));
        console.log(users);
    } else {
        //alert("Form invalid.");
    }
}

// const addUserBtn = document.getElementById("formButton");
//addUserBtn.addEventListener("click", addUser);
